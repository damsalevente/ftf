# Navigate to the followtheflow directory

## install custom gym envorinment

    change to gym-control dir
    pip install -e .

or

    ./install_env.sh

## run

    python agentos.py

## Description

Our goal is to teach an agent to control a water network system. To simulate the junctions deamnds (how much water does needed at each point of our system).

The agent controls the pump's speed, and it gets rewarded if it can satisfy each boundary conditions.

## Architecture

NetworkAgent class

-   uses epynet what is an is an object oriented wrapper around the EPANET 2.1,
-   holds the limits; since the solver don't know if a state is physically possible, we had to give an intervall
-   builds effeciency curves; From the given pump characteristic.
-   calculates reward

Gym env

-   uses networkagent
-   step, reset functions

### Action

The agent acts on a discrete space, it can increase, decrease or leave the pump speed alone. Step size can be adjusted, the speed limit is between 0.7 and 1.2. The speed is relative to the flow.

### Reward

The reward is built from 3 different things:

-   Junction head pressures
    For every junction, check if it's within the threshold(15,120)
    if yes, reward is zero -> no extra reward for it
    else punish with -1
    Then sums the rewards, and then scale it with the number of junctions. Therefore the reward is -1 if every junction demand is unsatisfied and it's 1 if everything is satisfied.

-   Speed difference

    From Nelder-mead method we get an optimized pump speed. Then we take the difference of the agent's speed the optimum speed.

-   Pump's effeciency:

    -   Calculating effeciency from E1 in the input file
    -   Call the function to get the flow, scale the flow with the speed

```python
def calc_eff(self):
    # get all pumps
    # return the mean eff.
    effs = []
    for pump in self.network.pumps:
        flow = self.network.pumps[pump.uid].flow
        logging.info('flow is: {}'.format(flow))

        speed = self.network.pumps[pump.uid].speed
        logging.info('speed is: {}'.format(speed))

        true_flow = float(flow/speed)

        result = self.eff(true_flow)
        logging.info('effeciency result : {}'.format(result))
        effs.append(result)

    return np.linalg.norm(np.array(effs))
```

# Evaluation

## Previous test result with DQN

    --------------------------------------
    | % time spent exploring  | 2        |
    | episodes                | 100      |
    | mean 100 episode reward | 3.9      |
    | steps                   | 989      |
    | success rate            | 0.0505   |
    --------------------------------------
    --------------------------------------
    | % time spent exploring  | 2        |
    | episodes                | 200      |
    | mean 100 episode reward | 3.9      |
    | steps                   | 1989     |
    | success rate            | 0.2      |
    --------------------------------------
    --------------------------------------
    | % time spent exploring  | 2        |
    | episodes                | 300      |
    | mean 100 episode reward | 4.2      |
    | steps                   | 2989     |
    | success rate            | 0.19     |
    --------------------------------------
    --------------------------------------
    | % time spent exploring  | 2        |
    | episodes                | 400      |
    | mean 100 episode reward | 5        |
    | steps                   | 3989     |
    | success rate            | 0.39     |
    --------------------------------------

pump will get closed if can't deliver head

## Results

# random agent

![random](followtheflow/random.png)

With steps plotted also

![random](followtheflow/randomstepstofinish.png)

# our proposed agent

![ours](followtheflow/our_method.png)

![ours](followtheflow/stepstofinish.png)
