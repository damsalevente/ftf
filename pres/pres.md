# Intro
- feladat ismertetése (hajgató email)  Bence 1 perc
# Epanet 
- mit akarunk elérni  1 perc
# Reinforcement learning
# Döntési feladatok 
- agent környezet reward 1.5
- Policy + miért  2 
# Our toy project with beer finder 
- első próba 
- epynet 
- milyen paraméterek 2 perc 
# Results  
- Ábrák 
# Future improvements 
  ## Big network
  ## Continous mode 
  ## Supervised learning 
  ## Planning agent 
# Kérdések ? 

# Levente
Először leegyszerűsítettük a problémát egy 1 dimenziós irányításra ahol az ugynök tudja változtatni a sebességét egy 1dimenziós tileseten. Minden lépésben megkapja a bot az aktuális poziciójának a pontját. Valahol középen volt egy nagy reward-os tileset, amit el kell érnie.

Ezen a toy projecten megismerkedtünk az openai gym keretrendszerével, használhatóságával, és számos ügynököt kipróbálhattunk rajta.

Ezután az eredeti hálózatot valósítottuk meg amihez az epynet python module-t használtuk, aminek a segítségével tudtunk futtatni szimulációkat egy vízhálózaton.

Episode-onként a vízigényeket(demandeket) változtatjuk, a szivattyú sebesség az előző állástól indul. 

A reward függvény a nyomásértékeket és a szivattyúk hatásfokából állt össze, amiket súlyoztunk. A helyes nyomásérték egy szigorúbb feltétel.  

Egy episode fix lépésszámból áll, aminél gyorsabban ki tud lépni nagy jutalommal, ha az optimális megoldástól egy bizonyos thresholdig tér el.

Az így definiált környezetben vizsgáltuk az ügynököket.

Továbbfejlesztési lehetőségként át lehetne térni folytonos környezetre, vagy egy nagyobb vízhálózatot használni, ahol több szivattyúállomás is van.
