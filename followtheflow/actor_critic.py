from keras import backend as K
from keras.layers import Dense, Input
from keras.models import Model
from keras.optimizers import Adam
import numpy as np

class Agent:
    def __init__(self, alpha, beta, gamma=0.99, n_actions = 3, layer1_size=64, layer2_size= 32, input_dims=24):
        self.alpha=alpha
        self.beta=beta
        self.gamma=gamma
        self.n_actions = n_actions
        self.layer1_size=layer1_size
        self.layer2_size=layer2_size
        self.input_dims = input_dims

        self.actor, self.critic, self.policy = self.build_agents()
        self.action_space = [x for  x in range(self.n_actions)]

    def build_agents(self):
        input1 = Input(shape=(self.input_dims,))
        delta = Input(shape=[1])
        dense1 = Dense(self.layer1_size, activation='relu')(input1)
        dense2 = Dense(self.layer2_size, activation='relu')(dense1)

        probs = Dense(self.n_actions, activation='softmax')(dense2)

        values= Dense(1, activation='linear')(dense2)

        def custom_loss(y_true, y_pred):
            out = K.clip(y_pred, 1e-8, 1-1e-8)
            log_lik = y_true * K.log(out)

            return K.sum(-log_lik*delta)
        actor = Model(inputs=[input1, delta], output=[probs])
        actor.compile(optimizer=Adam(lr=self.alpha), loss=custom_loss)

        critic = Model(inputs=[input1], output=[values])
        critic.compile(optimizer=Adam(lr=self.beta), loss='mean_squared_error')

        # no backprop
        policy = Model(inputs=[input1], output=[probs])

        return actor, critic, policy
    
    def choose_action(self, obs):
        obs = np.array(obs)
        state = obs[np.newaxis, :]
        probabilites = self.policy.predict(state)[0]
        action = np.random.choice(self.action_space, p=probabilites)

        return action

    def learn(self, state, action, reward, state_, done):
        state_ = np.array(state_)
        state = np.array(state)
        state = state[np.newaxis, :]
        state_ = state_[np.newaxis, :]

        critic_value_ = self.critic.predict(state_)
        critic_value = self.critic.predict(state)

        target = reward + self.gamma*critic_value_*(1-int(done))
        delta = target - critic_value

        actions = np.zeros([1, self.n_actions])
        actions[np.arange(1), action] = 1.0

        self.actor.fit([state, delta], actions,verbose=0)
        self.critic.fit(state, target,verbose=0)







