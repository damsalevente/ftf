from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras import backend as K
from tensorflow.keras import optimizers
from tensorflow.keras.optimizers import Adam
import gym
import tensorflow as tf
import numpy as np
from keras.utils import np_utils
import logging

logging.basicConfig(filename='gym.log',filemode='w', level=logging.INFO)


import numpy 

class ReinforceAgent:
    loss = []

    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.gamma = 0.99  # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.1
        self.freq = 5
        self.update = 0
        self.cnt = 0
        self.epsilon_decay = 1
        self.learning_rate = 0.0001
        self.model = self._build_model()
        self.build_train_fn()

    def _build_model(self):
        model = Sequential()
        model.add(Dense(256, input_dim=self.state_size, activation='tanh', kernel_initializer='glorot_normal'))
        model.add(Dense(256, activation='tanh', kernel_initializer='glorot_normal'))
        model.add(Dense(256, activation='tanh', kernel_initializer='glorot_normal'))
        model.add(Dense(256, activation='tanh', kernel_initializer='glorot_normal'))
        model.add(Dense(self.action_size, activation='softmax', kernel_initializer='glorot_normal'))
        return model

    def action(self, state ):
        # e used for epsilone before?  
        if np.random.rand() > self.epsilon:
            prob = self.model.predict(np.reshape(state, (1,self.state_size)))[0]
            a = np.argmax(prob)
            return a
        else:
            At = np.arange(0, self.action_size)
            prob = self.model.predict(np.reshape(state, (1, self.state_size)))[0]
            a = np.random.choice(a=At, p=prob)
            return a

    def build_train_fn(self):
        action_prob_placeholder = self.model.output
        action_onehot_placeholder = K.placeholder(shape=(None, self.action_size), name="action_onehot")
        discount_reward_placeholder = K.placeholder(shape=(None,), name="discount_reward")
        action_prob = K.sum(action_prob_placeholder * action_onehot_placeholder, axis=1)
        Pi_placeholder = K.placeholder(shape=(None,), name="Pi_placeholder")
        # Reinforce Algo
        # K.mean(K.square(y_pred - y_true)
        log_action_prob = K.log(tf.clip_by_value(action_prob, 0.01, 1))
        # loss = - log_action_prob * discount_reward_placeholder
        loss = - log_action_prob * Pi_placeholder
        loss = K.mean(loss)
        # Update with the gradients of the custom loss
        adam = optimizers.Adam(lr=self.learning_rate)
        updates = adam.get_updates(params=self.model.trainable_weights, loss=loss)
        # print(updates)
        # Make a Keras function for the process
        print(self.model.input)
        print(self.model.input)
        self.train_fn = K.function(inputs=[self.model.input, action_onehot_placeholder, Pi_placeholder], outputs=[],
                                   updates=updates)
        return 0

    def get_model(self):
        return self.model

    def predict(self, state):
        prob = self.model.predict(state)
        return prob

    def get_P(self, state):
        prob = self.model.predict(np.reshape(state, (1, 8)))[0]
        return prob

    # Custom fit
    def fit(self, S, A, Q):
        action_onehot = np_utils.to_categorical(A, num_classes=self.action_size)
        # print(S.shape,action_onehot.shape,discount_reward.shape)
        self.train_fn([S, action_onehot, Q])
        self.epsilon = self.epsilon * self.epsilon_decay
        return 0


class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.dict_memory = {}
        self.memory = []
        self.prior_memory = []
        self.gamma = 0.6  # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.05
        self.epsilon_decay = 0.998
        self.learning_rate = 0.00001
        self.model = self._build_model()
        self.model1 = self._build_model1()
        self.k = np.random.rand() / 10

    def _build_model(self):
        model = Sequential()
        model.add(Dense(256, input_dim=self.state_size, activation='tanh', kernel_initializer='glorot_normal'))
        model.add(Dense(256, activation='tanh', kernel_initializer='glorot_normal'))
        model.add(Dense(256, activation='tanh', kernel_initializer='glorot_normal'))
        model.add(Dense(self.action_size, activation='linear', kernel_initializer='glorot_normal'))
        model.compile(loss='mse',
                      optimizer=optimizers.Adam(lr=self.learning_rate), metrics=['accuracy'])
        return model

    def _build_model1(self):
        model = Sequential()
        model.add(Dense(256, input_dim=self.state_size, activation='tanh', kernel_initializer='glorot_normal'))
        model.add(Dense(256, activation='tanh', kernel_initializer='glorot_normal'))
        model.add(Dense(256, activation='tanh', kernel_initializer='glorot_normal'))
        model.add(Dense(self.action_size, activation='linear', kernel_initializer='glorot_normal'))
        model.compile(loss='mse',
                      optimizer=Adam(lr=self.learning_rate), metrics=['accuracy'])
        return model

    def set_weights(self):
        weights = self.model.get_weights()
        self.model1.set_weights(weights)

    def remember(self, state, action, reward, next_state, done):
        if len(self.dict_memory) >= 512 or len(self.memory) >= 512:
            n = np.random.randint(512, size=1)[0]
            keys = list(self.dict_memory.keys())
            self.dict_memory.pop(keys[n])
            # self.memory.pop(n)
        self.dict_memory[self.k] = (state, action, reward, next_state, done)
        # self.memory.append((state, action, reward, next_state, done))
        self.k = np.random.rand() / 10

    def batch_size(self):
        if len(self.dict_memory) < 64:
            return len(self.dict_memory)
        else:
            return 64

    def get_Q(self, state):
        Q = self.model.predict(np.reshape(state, (1, 8)))[0]
        # print("Q",Q)
        return Q

    def get_model(self):
        return self.model

    def predict(self, state):
        prob = self.model.predict(state)
        return prob

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            A = []
            act_values = list(self.model.predict(np.reshape(state, (1, 8)))[0])
            ppp = softmax(act_values)
            C = np.arange(len(act_values))
            B = list(act_values)
            # ppp = softmax(act_values)
            A.append(B.index(act_values.pop(act_values.index(max(act_values)))))
            A.append(B.index(act_values.pop(act_values.index(max(act_values)))))
            A.append(B.index(act_values.pop(act_values.index(max(act_values)))))
            # print(A)
            a = np.random.choice(a=C, p=ppp)
            return a
        else:
            act_values = self.model.predict(np.reshape(state, (1, 8)))
        return np.argmax(act_values[0])

        # returns action

    def greedy(self, state):
        act_values = self.model.predict(np.reshape(state, (1, 8)))
        return np.argmax(act_values[0])

    def replay(self, batch_size):
        """
        #Pure sorting based on TD targets
        if batch_size > 255:
            dist = len(self.dict_memory)-255
            self.dict_memory = dict(sorted(self.dict_memory.items()))
            minibatch = list(self.dict_memory.items())[dist:]
        else:
            minibatch = list(self.dict_memory.items())

        #Uniform sampling
        minibatch=random.sample(self.memory,batch_size)

        """
        # Stochastic rank/prob
        if batch_size > 63:
            distribution = []
            probs = list(self.dict_memory.keys())
            for i in range(len(probs)):
                distribution.append(probs[i] / sum(probs))
            minibatch = list(np.random.choice(probs, 64, p=distribution))
        else:
            minibatch = list(self.dict_memory.keys())

        states = []
        # print(minibatch)
        targets = []
        rankpi = []
        probpi = []
        # state1, action1, reward1, next_state1, done1
        for key in minibatch:
            target = self.dict_memory[key][2]
            if not self.dict_memory[key][4]:
                # target= self.dict_memory[key][2] + self.gamma*(np.amax(self.model.predict(np.reshape(self.dict_memory[key][3], (1, 8)))))
                target = self.dict_memory[key][2] + self.gamma * \
                         self.model1.predict(np.reshape(self.dict_memory[key][3], (1, 8)))[0][
                             np.argmax(self.model.predict(np.reshape(self.dict_memory[key][3], (1, 8)))[0])]
            target_f = self.model.predict(np.reshape(self.dict_memory[key][0], (1, 8)))
            # print("target",target)
            loss = abs(target_f[0][self.dict_memory[key][1]] - target)
            # print("Calculated target",target,"Predicted",target_f[0][self.dict_memory[key][1]],"Reward",self.dict_memory[key][2],"Loss",loss)

            # rankpi.append(pow(1/(len(minibatch)-cnt),0.6))
            probpi.append(pow((loss + 0.3), 1))
            target_f[0][self.dict_memory[key][1]] = target
            # self.dict_memory[loss] = self.dict_memory.pop(pi)
            states.append(np.reshape(self.dict_memory[key][0], (1, 8)))
            targets.append(target_f)
        # new_state = np.split(np.array(PRN_states),split)[0]
        # new_targets = np.split(np.array(PRN_targets),split)[0]
        # print(targets)
        self.model.fit(states[0], targets[0], epochs=1, verbose=0)
        seen = []
        for i in range(len(minibatch)):
            seen.append(minibatch[i])
            if seen.count(minibatch[i]) == 1:
                prob = probpi[i] / sum(probpi)
                # rank = rankpi[i] / sum(rankpi)
                f = self.dict_memory.pop(minibatch[i])
                self.dict_memory[prob] = f
                # self.dict_memory[rank] = self.dict_memory.pop(minibatch[i][0])
        # self.model.fit(new_state[0], new_targets[0], epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay


env = gym.make('gym_control:control-v0')
print(env.action_space.n)
print(env.observation_space.shape)
q = [3.3, 4.768, 17.5, 5.73, 3.0, 4.77, 7, 3.0, 16.25, 6.42, 3.12]
print(len(q))
Critic = DQNAgent(24, 3)
mod = Critic.get_model()
print(mod.summary())
Actor = ReinforceAgent(24, 3)
actions = np.arange(Actor.action_size)
d = False

num_episodes = 1000

def discount(R):
    discounted = []
    running = 0
    gamma = 0.99
    r = list(reversed(R))
    for j in range(len(R)):
        running = r[j] + gamma * running
        discounted.append(running)
    return list(reversed(discounted))

event_buffer = [] 
log = [] 
score_history = [] 
for i in range(num_episodes):
    done = False
    observation = env.reset()
    R = [] 
    S = [] 
    A = [] 
    # one episode
    rewards = [] 
    j = 0 
    while not done:
        
        action = env.action_space.sample()
        observation_, reward, done, info = env.step(action)
        event_buffer.append(info)
        #? Critic.act()
        R.append(reward)
        S.append(observation)

        A.append(action)




        observation = observation_
        j += 1 



    score = discount(R)
    score = np.array(score)
    S = np.array(S)
    A = np.array(A)
    Actor.fit(S, A, score)

    print('steps: {} score : {}'.format(j,score[-1]))
    score_history.append(score[-1])
    log.append(j)

filename = 'random.png'
import matplotlib.pyplot as plt

x = None
window = 5
N = len(score_history)
running_avg = np.empty(N)
for t in range(N):
    running_avg[t] = np.mean(score_history[max(0, t - window):(t + 1)])
if x is None:
    x = [i for i in range(N)]

plt.ylabel('Score')
plt.xlabel('Game')
plt.plot(x, running_avg)
plt.savefig(filename)

filename= 'randomstepstofinish.png'
x = None
window = 5
N = len(log)
running_avg = np.empty(N)
for t in range(N):
    running_avg[t] = np.mean(log[max(0, t - window):(t + 1)])
if x is None:
    x = [i for i in range(N)]

plt.ylabel('Steps to solve')
plt.xlabel('Game')
plt.plot(x, running_avg)
plt.savefig(filename)
for element in event_buffer:
    print(element['dataset'])
