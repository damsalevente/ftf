from keras.layers import Dense, Activation, Input
from keras.models import Model, load_model
from keras.optimizers import Adam
import keras.backend as K
import numpy as np

class Agent(object):

    def __init__(self, alpha, gamma=0.99, n_actions = 3,
                layer1_size=64, layer2_size=64, input_dims=24,
                fname = 'reinforce.h5'):
        self.gamma = alpha
        self.lr = alpha
        self.G = 0
        self.input_dims = input_dims
        self.fc1_dims = layer1_size
        self.fc2_dims = layer2_size
        self.n_actions = n_actions
        self.filepath = fname
        self.state_memory = []
        self.action_memory = [] 
        self.reward_memory = [] 

        # batch episodes

        self.policy, self.predict = self.build_function()

        self.action_space = [x for x in range(n_actions)]


    def build_function(self):
        input1 = Input(shape=(self.input_dims,))
        advantages = Input(shape=[1])
        dense1 = Dense(self.fc1_dims, activation='relu')(input1)
        dense2 = Dense(self.fc2_dims, activation = 'relu')(dense1)
        probs = Dense(self.n_actions, activation='softmax')(dense2)

        def custom_loss(y_true, y_pred):
            out = K.clip(y_pred, 1e-8, 1-1e-8)
            log_lik = y_true * K.log(out)

            return K.sum(-log_lik*advantages)

        policy = Model(inputs=[input1, advantages], output=[probs])
        policy.compile(optimizer=Adam(lr=self.lr), loss=custom_loss)

        predict = Model(inputs=[input1], output=[probs])

        return policy, predict

    def choose_action(self, observation):
        
        state = np.array(observation)[np.newaxis,:]

        probabilities = self.predict.predict(state)[0]
        action = np.random.choice(self.action_space, p=probabilities)

        return action

    def store_transition(self, observation, action, reward):
        self.action_memory.append(action)
        self.state_memory.append(observation)
        self.reward_memory.append(reward)

    def learn(self):
        state_memory = np.array(self.state_memory)
        action_memory = np.array(self.action_memory)
        reward_memory = np.array(self.reward_memory)

        actions = np.zeros((len(action_memory), self.n_actions))
        actions[np.arange(len(action_memory)), action_memory] = 1

        G = np.zeros_like(reward_memory)

        for t in range(len(reward_memory)):
            G_sum = 0 
            discount = 1
            for k in range(len(reward_memory)):
                G_sum += reward_memory[k] * discount
                discount *= self.gamma
            
            G[t] = G_sum

        mean = np.mean(G)
        std = np.std(G) if np.std(G) > 0 else 1

        self.G = (G-mean)/std

        cost = self.policy.train_on_batch([state_memory, self.G], actions)

        self.state_memory = [] 
        self.action_memory = []
        self.reward_memory = [] 

        # return cost 
    
    def save_model(self):
        self.policy.save(self.filepath)

    def load_model(self):
        self.policy = load_model(self.filepath)



import gym 
import matplotlib.pyplot as plt

if __name__ == '__main__':
    agent = Agent(alpha=0.001, input_dims =24, gamma=0.99, n_actions=3)
    minibatch = 32
    env = gym.make('gym_control:control-v0')

    score_history = []
    n_episodes = 800

    for i in range(n_episodes):
        done = False
        score = 0 
        observation = env.reset()
        j = 0 
        for k in range(minibatch):
            while not done:
                action = agent.choose_action(observation)
                observation_, reward, done, info = env.step(action)
                agent.store_transition(observation, action, reward)
                observation = observation_
                score += reward
                j += 1 

        score_history.append(score)

        agent.learn()
        
        print('steps: {} episode {} score {} average_score: {}'.format(j, i, score, np.mean(score_history[-10:])))

    filename = 'basic'
    import matplotlib.pyplot as plt

    x = None
    window = 5
    N = len(score_history)
    running_avg = np.empty(N)
    for t in range(N):
        running_avg[t] = np.mean(score_history[max(0, t - window):(t + 1)])
    if x is None:
        x = [i for i in range(N)]

    plt.ylabel('Score')
    plt.xlabel('Game')
    plt.plot(x, running_avg)
    plt.savefig(filename)


            

