import logging

import gym
import numpy as np
from actor_critic import Agent

np.random.seed(1)

logging.basicConfig(filename='gym.log', level=logging.INFO, filemode='w')

env = gym.make('gym_control:control-v0')

if __name__ == '__main__':
    agent = Agent(alpha=0.001, beta=0.0005, layer1_size=16, layer2_size=8)
    score_history = []
    num_episodes = 500

    for i in range(num_episodes):
        done = False
        score = 0
        observation = np.array(env.reset())
        j = 0
        while not done:
            action = agent.choose_action(observation)
            observation_, reward, done, info = env.step(action)
            # print('Action: {} Optimal action: {}'.format(action,info['optimal_action']))
            agent.learn(observation, action, reward, observation_, done)
            observation = observation_
            score += reward
            j += 1
        print('episode finished with {} steps'.format(j))

        score_history.append(score)
        avg_score = np.mean(score_history[-10:])
        print('episode {} score {} average score: {}'.format(i, score, avg_score))

    filename = 'custom_gym_history_lotofmod'
    import matplotlib.pyplot as plt

    x = None
    window = 5
    N = len(score_history)
    running_avg = np.empty(N)
    for t in range(N):
        running_avg[t] = np.mean(score_history[max(0, t - window):(t + 1)])
    if x is None:
        x = [i for i in range(N)]

    plt.ylabel('Score')
    plt.xlabel('Game')
    plt.plot(x, running_avg)
    plt.savefig(filename)
