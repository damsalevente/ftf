import gym
from scipy import stats
import numpy as np
from gym import error, spaces, utils
from gym.utils import seeding
import logging
from epynet import Network
from scipy.optimize import minimize, differential_evolution
from scipy import stats
import pandas as pd
import matplotlib.pyplot as plt
import logging

np.random.seed(1337)
class NetworkAgent:

    def __init__(self, filename='anytown_master.inp'):
        """
        filename: string, path to the epanet input file (.inp)
        Creates an epynet Network instance
        calculates effciency from curves
        sets speed, junction limits for gym
        randomize demands
        change speed 
        solve network or run()? 
        add tank to state
        add tank 1/(inflow - outflow ) as reward
        """

        # load network
        self.filename = filename
        self.network = Network(filename)
        self.network.solve()

        # for control
        self.speed_limit = (0.7, 1.2)

        # danger zone
        self.junction_head_threshold = (15, 120)

        # function to get the effeciency of the pump(s)
        self.eff = None

        # store optimal speed's effeciency too
        # comes from nelder mead
        self.max_effeciency = 0.0
        self.start_effeciency = 0.0

        # speed increase/decrease resolution
        # able to traverse half the interval
        # 1/2 * (1.2 - 0.7)/num_steps 
        self.stepsize = 0.0025

        self._calc_effeciency('E1')
        # Demand randomizer
        self.total_demand_lo = 0.3
        self.total_demand_hi = 1.1
        self.demandRandomizer = self.build_truncnorm_randomizer(lo=.7,
                                                                hi=1.3,
                                                                mu=1.0,
                                                                sigma=1.0)
        # result for the current demand
        self.res = None

    def build_truncnorm_randomizer(self, lo, hi, mu, sigma):
        randomizer = stats.truncnorm(
            (lo - mu) / sigma, (hi - mu) / sigma, loc=mu, scale=sigma)
        return randomizer

    def sumOfDemands(self):
        """
        higher demands for testing 
        """
        return 19 * 200

    def randomize_demands(self):
        target_sum_of_demands = self.sumOfDemands() * (self.total_demand_lo +
                                                       np.random.rand() * (self.total_demand_hi - self.total_demand_lo))
        sum_of_random_demands = 0
        for junction in self.network.junctions:
            junction.basedemand = (self.network.junctions[junction.uid].demand *
                                   self.demandRandomizer.rvs())
            sum_of_random_demands += junction.basedemand
        for junction in self.network.junctions:
            junction.basedemand *= target_sum_of_demands / sum_of_random_demands

    def speed_randomizer(self):
        return np.random.random_integers(7, 12) * 1 / 10

    def optimal_action(self):
        """
        0: speed up
        1: step down
        2: stay 
        """
        threshold = 0.10
        action = -1
        diff = self._speed_diff()
        # within threshold?
        if diff < threshold:
            action = 2
        else:
            # ground truth is bigger than predicted value
            if diff > 0:
                # speed up
                action = 0
            else:
                # slow down
                action = 1

        return action

    def _calc_effeciency(self, curve_id):
        """
        We can calculate the effeciency from the flow
        for X amount of flow, the pump will work in y eff.
        Find the effeciency curve from the input file, get the x-y values
        and create a function from it with polyfit

        Note: In epanet, the e1 curve wasn't used for pumps 
        Return the created function 
        """
        curve = None
        for x in self.network.curves:
            if curve_id in x.uid:
                flow = [value[0] for value in x.values]
                y = [value[1] for value in x.values]
                curve = np.poly1d(np.polyfit(flow, y, 4))
                logging.info('curve setup finished')
        if not curve:
            logging.info('no curve found with id {}'.format(curve_id))
        self.eff = curve
        x = [i for i in range(1000)]
        plt.plot(x,self.eff(x))
        plt.show()


    def find_opt(self, x):
        """
        x : pumps' speed
        returns: effeciency
        """
        x_i = x
        # pump speed can't be set oob 
        # manipulate the function directly
        for pump in self.network.pumps:
            if x_i <= 0.7:
                return -100 
            if x_i >= 1.2:
                return -100

            self.network.pumps[pump.uid].speed = x_i

        # logging.info('speed {}'.format(self.network.pumps.speed))

        self.network.solve()

        eff = self.calc_eff()
        # use reward function instead
        # reward = self.reward()

        # - sign because we need the maximum value
        # changed from eff
        return -eff

    def reset(self):
        # reinit the whole network
        self.network = Network(self.filename)
        self.network.solve()
        self._calc_effeciency('E1')

        # change the speed
        # self.change_speed(self.speed_randomizer())
        #logging.info("next reset initial speed")

        # change the demands
        self.randomize_demands()

        logging.info('Demand for this episode:{}'.format(
            self.network.junctions.demand))

        # optimize
        # initial guess is the previous speed
        original_speed = self.network.pumps.speed
        # x0 = [1.1, 1.1]  # self.network.pumps.speed.values
        x0 = 0.8  # self.network.pumps.speed.values

        # for one value
        self.res = minimize(self.find_opt, x0,
                            method='TNC',
                            bounds=[(0.7,1.2)],
                            options={'disp': False}
                            )
        self.network.solve()
        self.max_effeciency = self.calc_eff()  # store best possible effeciency

        logging.info('Result from nelder method: {}, eff.: {}'.format(
            self.res.x, self.max_effeciency))
        # reset the speed to it's original value, so the agent should start from the same value
        self.network.pumps.speed = original_speed
        self.network.solve()
        self.start_effeciency = self.calc_eff()

    def step(self):
        """
        call epynet solve function on the network
        it will solve it for one timestep
        The network's properties should be 
        """

        self.network.solve()
        logging.info('____\nstep info: speed:{} - flow - {} - pressures:{}\n__________'.format(self.network.pumps.speed.values,
                                                                                               self.network.pumps.flow.values,
                                                                                               self.network.junctions.head.values))

    def calc_eff(self):
        """
        From the effeciency curve, calculate the pumps' effeciency
        """
        effs = []
        # for every pump calculate the true flow ( scale it with speed) and create a eff. list
        for pump in self.network.pumps:
            flow = self.network.pumps[pump.uid].flow

            speed = self.network.pumps[pump.uid].speed
            logging.info(
                'pump {} - flow: {} - speed:{}'.format(pump.uid, flow, speed))

            true_flow = float(flow / speed)

            result = self.eff(true_flow)
            if result < 0:
                result = 0 
            logging.info('effeciency result : {}'.format(result))
            effs.append(result)

        logging.info('effeciency array{}'.format(effs))

        return np.sqrt(sum([x**2 for x in effs])) 

    def _within_th(self, x):
        """
        Check a junction head if it's in the threshold
        It gets -10 reward if it's out of range, else 0 (no reward for doing the minimum)

        """
        if ((x < self.junction_head_threshold[1]) and (x > self.junction_head_threshold[0])):
            return 0
        return -1

    def _flowdiff(self):
        """
        if within threshold -> 0
        else -1
        sum them
        scale to [-1, 0]
        if all pipes are good, reward is 0
        """
        # get the junction heads
        junctions = list(self.network.junctions.head)
        logging.info('junctions from {}'.format(junctions))
        n = len(junctions)

        return float(sum([self._within_th(x) for x in junctions]))/100

    def _speed_diff(self):
        """
        Calculate the difference between the optimal effeciency (calculated with tnt)
        and the current speed 
        the values needs to be normed before subtraction
        """
        y_hat = self.calc_eff()
        return (self.max_effeciency - y_hat)

    def effeciency_reward(self):
        current = self.calc_eff()
        res = 0
        bad = False
        if current < self.start_effeciency:
            res = -1
            bad = True
        else:
            # the more you improved from your place
            res = (current - self.start_effeciency)/self.max_effeciency
        return res, bad

    def inoutflow(self):
        """ reward based on tank level
            punish the  difference between inflow and outflow 
        """
        diffs = []
        for tank in self.network.tanks:
            diffs.append(np.abs(
                self.network.tanks[tank.uid].inflow - self.network.tanks[tank.uid].outflow))
        return -np.linalg.norm(np.array(diffs))

    def reward(self):
        """
        Reward built from 3 things: effeciency, head pressure out of threshold and speed difference from optimum(this is for testing)
        """
        bad = False
        
        r_from_eff = self.calc_eff() # taking their dot product wouldn't make any sense, since the individual pumps not coupled the same as machine with multiple component
        r_from_delta = self._flowdiff()
        return r_from_eff + r_from_delta, bad # + r_from_tanks # r_from_delta

    def junction_head(self):
        # logging.info(self.network.junctions.head)
        return [x * 0.007 for x in list(self.network.junctions.head)]
        #return [x for x in list(self.network.junctions.head)]

    def tanks(self):
        return list(self.network.tanks.level)

    def change_speed(self, speed):
        """
        helper function, not used currently
        """
        for pump in self.network.pumps:
            self.network.pumps[pump.uid].speed = speed

    def action_increase_speed(self):
        """
        Increase the pump's value with stepsize
        The current agent can only either increase or decrease all speeds
        """

        # if the agent predicted wrong, we should punish it
        bad = False
        for pump in self.network.pumps:
            if self.network.pumps[pump.uid].speed + self.stepsize < self.speed_limit[1]:
                self.network.pumps[pump.uid].speed += self.stepsize
            else:
                bad = True
        logging.info('Pump speed set to {}'.format(self.network.pumps.speed))
        return bad

    def action_decrease_speed(self):
        """
        Decrease pumps speed with stepsize
        Returns True if wrong value was given
        """
        bad = False
        for pump in self.network.pumps:
            if self.network.pumps[pump.uid].speed - self.stepsize > self.speed_limit[0]:
                self.network.pumps[pump.uid].speed -= self.stepsize
            else:
                bad = True
        logging.info('Pump speed set to {}'.format(self.network.pumps.speed))

        return bad


class ControlEnv(gym.Env):
    metadata = {'render.modes': ['human']}
    """ 
    Actions: 
    Type: Dicrete(2)
    NUM Action
    0   increase speed 
    1   decrease speed 
    2   don't change

    note: we manipulate all pumps with the same action 

    State: 
    22 junction head
    2 pump speed 

    Reward:
    Effeciency of the pump(s)
    """

    def __init__(self):

        # increase/decrease speed or don't change
        # dqn
        self.action_space = spaces.Discrete(3)

        # Agent has steps available time to get to the right speed
        self.steps_available = 100

        # amount of steps when the agent was in the optimum
        self.counter = 0

        # envorinment
        self.network = NetworkAgent()

        self.epsilon = self.network.stepsize

        # create the effeciency curve
        self.network._calc_effeciency('E1')

        logging.info('effeciency curves set')

        self.n = len(self.network.junction_head())
        # thresholds for heads
        self.min = self.network.junction_head_threshold[0]
        self.max = self.network.junction_head_threshold[1]  

        # thresholds for speed
        self.min_speed = [0.7]
        self.max_speed = [1.2]

        j_min = list(self.min for i in range(self.n)) 
            
        j_max = list(self.max for i in range(self.n)) 

        minarr = j_min + self.min_speed
        maxarr = j_max + self.max_speed

        self.low = np.array(minarr)
        self.high = np.array(maxarr)

        # building the observation space
        self.observation_space = spaces.Box(low=self.low, high=self.high)

        self.viewer = None
        logging.info('action space {}'.format(self.action_space))
        logging.info('observation space {}'.format(self.observation_space))

    def step(self, action):
        """ 
        one step: solve the network with the new action
        it will generate new pressures
        Calculate the reward 

        returns observation, reward, done, info 
        """

        info = {}
        info['is_success'] = False

        done = False
        # decrease stepsize
        self.steps_available -= 1

        # every extra step is bad
        reward = -1

        opt_action = self.network.optimal_action()
        logging.info('optimal action was : {}'.format(opt_action))
        info['optimal_action'] = opt_action

        # make the move, increase, decrease speed
        err = False
        if (action == 0):
            err = self.network.action_increase_speed()
            logging.info('action was speed')
            info['action'] = 'speed '
            self.counter = 0

        elif action == 1:
            err = self.network.action_decrease_speed()
            logging.info('action was slow')
            info['action'] = 'slow'
            self.counter = 0
        else:
            # increase the counter if it stayed in the optimum, or throw away the stored good counter
            if opt_action == 2:
                self.counter += 1
            else:
                self.counter = 0

            logging.info('action was stay')
            info['action'] = 'stay'

        # failed if illegal move happaned
        if err:
            reward += -1 
            done = True

        logging.info('FROM STEP')
        # solve the network with the new speed
        self.network.step()

        # earn the reward
        # mistake was here -> reinit reward
        r, bad = self.network.reward()
        reward += r
        if bad:
            reward += -1 
            done = True 

        logging.info('reward was {}'.format(reward))

        # check if we were in the optimum for 5 consecutive move
        if self.counter > 5:
            # big reward 
            r, _ =  self.network.effeciency_reward()
            reward += self.steps_available
            reward += r
            info['is_success'] = True
            done = True

        # check if we out of moves
        if self.steps_available == 0:
            r,_ = self.network.effeciency_reward()
            reward += r
            done = True

        # create the new state from speed values and junction heads
        speed = self.network.network.pumps.speed

        j_heads = self.network.junction_head()
        state = j_heads + list(speed)

        logging.info('state is {}'.format(state))

        # this is for the supervised learning
        state_arr = state + [opt_action, self.network.max_effeciency, self.network.start_effeciency, self.network.calc_eff(), speed.values]

        # what should've happened
        info['dataset'] = state_arr

        return state, reward, done, info

    def reset(self):
        logging.info('RESET EVENT')
        """
        returns observation and resets env state
        """
        self.steps_available = 100

        self.counter = 0

        self.network.reset()

        # build the state
        speed = self.network.network.pumps.speed.values
        logging.info('Reset speed set to: {}'.format(speed))

        j_heads = self.network.junction_head()
        state = j_heads + list(speed)

        logging.info('state is  {}'.format(state))

        return state

    def render(self, mode='human', close=False):
        raise NotImplementedError
