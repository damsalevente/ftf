import numpy
import pandas
import keras
from keras.models import Sequential
from matplotlib import pyplot
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from scipy.stats import uniform
from sklearn.linear_model import Ridge
from sklearn.model_selection import RandomizedSearchCV
import os
from numpy import set_printoptions
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from keras.constraints import maxnorm
from keras.layers import Dropout
from sklearn.metrics import accuracy_score
from sklearn.externals import joblib
from keras.models import model_from_json
#saveing model



# fix random seed for reproducibility
Xx=[]
seed = 7
numpy.random.seed(seed)
dataframe = pandas.read_csv("training.csv", header=None)
dataset = dataframe.values
X = dataset[:, 0:8].astype(float)
Y = dataset[:, 8]
set_printoptions(precision=5)

"""
scaler = StandardScaler().fit(X)
rescaledX = scaler.transform(X)
# summarize transformed data

joblib.dump(scaler, 'Scaler_trajectory.joblib')
"""


# encode class values as integers
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_y = np_utils.to_categorical(encoded_Y)

model=Sequential()

adam = keras.optimizers.Adam(lr=0.000001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
# define baseline model
adadelta=keras.optimizers.Adadelta(lr=0.01, rho=0.95, epsilon=None, decay=0.0)
rm=keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
def baseline_model():
    model.add(Dense(128, input_dim=8, activation='tanh',kernel_initializer='glorot_normal'))
    model.add(Dense(128,activation='tanh',kernel_initializer='glorot_normal'))
    model.add(Dense(128,activation='tanh',kernel_initializer='glorot_normal'))
    model.add(Dense(128,activation='tanh',kernel_initializer='glorot_normal'))
    model.add(Dense(7, activation='softmax'))
    model.compile(loss='categorical_crossentropy',optimizer='adam', metrics=['accuracy'])
    return model
history=KerasClassifier(build_fn=baseline_model, epochs=300, batch_size=128, verbose=2)
kfold = KFold(n_splits=4, shuffle=True, random_state=seed)
results = cross_val_score(history, X, dummy_y)
print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))

#256 99.64 std 0.03  16x16 99.81 0.02  8x8  99.85 0.04  4x4 99.86 0.02

#saveing model
model_json = model.to_json()
with open("trajectory_4x4.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("trajectory_4x4.h5")
print("Saved model to disk")
